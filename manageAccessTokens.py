#!/usr/bin/env python

from requests import post, codes
from time import time

from aop_lib.ourConfig import getConfig

ourConfig = getConfig('DataExport')

ourToken = None
tokenExpiry = None

def getAccessToken(solution):
    global tokenExpiry
    global ourToken
    if tokenExpired(tokenExpiry):

        ourToken, tokenExpiry = getNewAccessToken(solution)

    return ourToken

def tokenExpired(expiry):
    if expiry is None:
        return True

    if time() > expiry:
        return True

    return False

def getNewAccessToken(solution):
    global ourConfig

    if ourConfig is None:
        ourConfig = getConfig(solution)

    payload = {
        "client_id": ourConfig.get("client_id"),
        "client_secret": ourConfig.get("client_secret"),
        "audience": "https://api.cloud.papercut.com",
        "grant_type": "client_credentials"
    }

    headers = { 'content-type': "application/json" }

    resp = post("https://myaccount.papercut.com/oauth/token", json=payload, headers=headers)

    print("getting a new access token")
    print(f"payload  {payload}")
    if resp.status_code == codes.ok:
        
        return resp.json()['access_token'], resp.json()['expires_in'] + time()

    else:
        print(resp)
        resp.raise_for_status()

if __name__ == "__main__":
    print(getNewAccessToken())
