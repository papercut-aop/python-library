#!/usr/bin/env python

from json import load as jsonload, dump as jsondump
from logging import critical, debug, basicConfig, info, DEBUG
from sys import exc_info
from firebase_admin import credentials, firestore, initialize_app, exceptions

basicConfig(level=DEBUG)

def updateLastProcessDate(orgId, lastProcessDate, jobType, firebaseInfo):

    orgData = getCustomer(orgId, firebaseInfo)

    if orgData is None:
        critical(f"can't find data for customer {orgId}")
        return "ERROR"

    # orgData['dataExportSettings'][jobType] = {'lastProcessDate' : lastProcessDate }

    if 'dataExportSettings' not in orgData :
        orgData['dataExportSettings'] = {}
        
    if 'lastProcessDate' not in orgData['dataExportSettings'] : 
        orgData['dataExportSettings']['lastProcessDate']  = {}

    orgData['dataExportSettings']['lastProcessDate'][jobType] = lastProcessDate

    debug(f"want to update customer record to {orgData}")

    return storeCustomer(orgId, orgData, firebaseInfo)


def removeCustomer(orgId, productId, firebaseInfo):

    cred = credentials.Certificate(firebaseInfo)

    try:
        default_app = initialize_app(cred)
    except ValueError:
        pass

    db = firestore.client()
    try:
        customer_ref = db.collection('customers')
    except:
        critical("Failed to locate customers collection")

    #TODO Check that this is for the corrrect product.

    try:
        customer_ref.document(orgId).delete()
        debug(f"Deleted customer record: orgId = {orgId}")
        
    except exceptions.FirebaseError as ex:
        info(f"Could not delete valid customer record: orgId = {orgId}, exception = {ex}")

    return None

def storeCustomer(orgId, customerData, firebaseInfo):

    cred = credentials.Certificate(firebaseInfo)
    try:
        default_app = initialize_app(cred)
    except ValueError:
        pass

    db = firestore.client()
    customer_ref = db.collection('customers')
    try:
        customer_ref.document(orgId).set(customerData)
    except exceptions.FirebaseError as ex:
        info(f"Could not write valid customer record: rec = {customerData}, exception = {ex}")

    return None

def getCustomer(orgId, firebaseInfo):

    cred = credentials.Certificate(firebaseInfo)
    try:
        default_app = initialize_app(cred)
    except ValueError:
        pass

    db = firestore.client()
    customer_ref = db.collection('customers')
    try:
        return {**customer_ref.document(orgId).get().to_dict(), **{"orgId" : orgId }}
    except:
        info(f"getCustomerList(): Could not locate valid customer database file: exception {exc_info()[0]}")
        return None

def getCustomerList(myProductId, firebaseInfo):

    cred = credentials.Certificate(firebaseInfo)
    try:
        default_app = initialize_app(cred)
    except ValueError:
        pass

    db = firestore.client()
    customer_ref = db.collection('customers')

#    debug(f"Looking for customers using product {myProductId}")

    d = [{**doc.to_dict(), **{"orgId" : doc.id}} for doc in customer_ref.where(u'myProductId', u'==', myProductId).stream()]

    debug(f"Un Filtered customer list is \n {d}")

    return d
