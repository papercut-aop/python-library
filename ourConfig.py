#!/usr/bin/env python

from configparser import ConfigParser
from logging import critical, debug
from sys import argv, exit

def getConfig(productID = "DEFAULT", configFileName = "./conf/config.ini"):
    
    debug(f"Looking for {productID}")

    try:
        c = ConfigParser()

        c.read_file(open(configFileName))

        d =  dict(c.items(productID))

        d["product-id"] = productID

        return d

    except FileNotFoundError:
        critical('No config found')
        exit(-1)

    except Exception as err:
        critical(type(err).__name__)
        exit(-1)


if __name__ == "__main__":

    productID = argv[1] if len(argv) > 1 else "DEFAULT"
    ourConfig = getConfig(productID)
    print(ourConfig)
